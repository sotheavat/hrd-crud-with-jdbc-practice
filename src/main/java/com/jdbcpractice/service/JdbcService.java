package com.jdbcpractice.service;

import com.jdbcpractice.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JdbcService {
    public static Connection connect(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Loading driver successfully!");
        String URL = "jdbc:postgresql://localhost:5432/testdb";
        Connection connection = null;
        try {
             connection = DriverManager.getConnection(URL, "postgres", "AveryStrongPassword");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("Connected to database successfully!");
        return connection;
    }
    public static void findAll(Connection connection){
        String getAll = "SELECT * FROM student";
        Statement stmt = null;
        ResultSet allStudent = null;
        List<Student> studentList = new ArrayList<>();
        try {
            stmt = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            allStudent = stmt.executeQuery(getAll);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        while (true){
            try {
                if (!allStudent.next()) break;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            try {
                System.out.println(Integer.valueOf(allStudent.getString(1))+" "+allStudent.getString(2)+" "+Double.valueOf(allStudent.getString(3)));
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

        }
    }
    public static void deleteById(Connection connection){
        Scanner scanner = new Scanner(System.in);

        Statement stmt = null;
        int id = 0;
        while (true){
            try {
                System.out.print("Please enter ID : ");
                id = scanner.nextInt();
            }catch (Exception e){
                System.out.println("Invalid input! ");
                continue;
            }

            break;
        }

        String delete = "DELETE\n" +
                "FROM student\n" +
                "WHERE id = "+id+";";
        try {
            stmt = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
             stmt.executeUpdate(delete);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("after Delete : ");
        findAll(connection);
    }
    public static void saveById(Connection connection){
        Scanner scanner = new Scanner(System.in);
        String name;
        double score;
        System.out.print("Enter name:");
        name = scanner.next();
        while (true){
            try {
                System.out.print("Enter score: ");
                score = scanner.nextDouble();
            }catch (Exception e){
                continue;
            }
            break;
        }
        String stInsert = "INSERT INTO student (name, score) VALUES ('"+name+"', "+score+")";
        Statement statement2 = null;
        try {
            statement2 = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            statement2.executeUpdate(stInsert);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("after insert! ");
        findAll(connection);
    }
    public static void updateById(Connection connection){
        Scanner scanner = new Scanner(System.in);
        String name;
        double score;
        int id;
        while (true){
            try {
                System.out.print("Enter id: ");
                id = scanner.nextInt();
            }catch (Exception e){
                System.out.println("Invalid Input!");
                continue;
            }
            break;
        }
        System.out.print("Enter name:");
        name = scanner.next();
        while (true){
            try {
                System.out.print("Enter score: ");
                score = scanner.nextDouble();
            }catch (Exception e){
                continue;
            }
            break;
        }
        String updateString = "UPDATE student\n" +
                "SET name  = '"+name+"',\n" +
                "    score = "+score+"\n" +
                "WHERE id = "+id;
        Statement statement2 = null;
        try {
            statement2 = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            statement2.executeUpdate(updateString);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("after update! ");
        findAll(connection);
    }
}
